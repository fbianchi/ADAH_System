'''
Created on 23 ago 2017

@author Matteo Olivato <matteo.olivato@studenti.univr.it>
@author Federico Bianchi <federico.bianchi@studenti.univr.it>

'''
import os
import glob
from adah_model.JClass import JClass
from adah_controller import adah_util

class AdahModel(object):
    '''
    classdocs
    '''
    
    # lista dei descrittori per la generazione delle funzioni di checksum
    random_checksum_functions_descriptors = []
    
    # lista dei metodi disponibili per iniettate le chiamate alle funzioni di checksum [nome_file.java, nome_metodo, riga_del_file]
    activity_rcf_call_injection_points = []
    
    def __init__(self, android_project_path, original_checksum_function_template, original_checksum_function_imports):
        '''
        Constructor
        '''
        # path del progetto android
        self.android_project_path = android_project_path
        # path del file della funzione di checksum originale sotto forma di template
        self.original_checksum_function_template = original_checksum_function_template
        # path del file degli import necessari alla funzione di checksum originale
        self.original_checksum_function_imports = original_checksum_function_imports
        # lista dei path dei png che saranno accessibili esternalmente facendo l'unzip dell'apk (cartella /res del progetto)
        self.android_valid_pngs_paths = ("drawable/", "layout/", "mipmap-hdpi/", "mipmap-mdpi/", "mipmap-xhdpi/", "mipmap-xxhdpi/", "mipmap-xxxhdpi/", "values/")
        # lista dei path delle classi android non utilizzabili
        self.android_invalid_classes_paths = ("generated/", "test/", "androidTest/")
        
        # lista dei file java che contengono le classi del progetto android
        self.android_project_classes_paths = self.findClassesPaths()
        # lista degli oggetti JClass creati a partire dai file java del progetto e aggiorno i paths
        self.android_project_classes, self.android_project_classes_paths = self.getAndroidProjectClasses()
        # lista degli oggetti JClass che si riferiscono ad Activity creati a partire dai file java del progetto
        self.android_project_activity_classes = self.getAndroidProjectActivityClasses()
        
        # lista dei path dei png presenti nel progetto android
        self.android_project_pngs_paths = self.findValidPngsPaths()
        # file che conterrà i descrittori dei fiel png iniettati utili durante la fase di firma dell'apk
        self.png_file_injection_descriptors = self.android_project_path+".pngs_injector_descriptors.txt"
        
        # lista dei descrittori InjectionDescriptor per l'iniezione
        self.injection_descriptors_list = [];
        
    '''
    funzione che trova la lista dei file java nel progetto
    '''
    def findClassesPaths(self):
        return [y for x in os.walk(self.android_project_path) for y in glob.glob(os.path.join(x[0], '*.java')) if all(z not in y for z in self.android_invalid_classes_paths)]
       
    '''
    funzione che trova la lista delle immagini png valide del progetto
    '''    
    def findValidPngsPaths(self):
        invalid_png_paths = ["debug/","build/"]
        res = [y for x in os.walk(self.android_project_path) for y in glob.glob(os.path.join(x[0], '*.png')) if any(z in y for z in self.android_valid_pngs_paths)]
        return [x for x in res if all(y not in x for y in invalid_png_paths)]
        
    '''
    funzione che ritorna la lista di oggetti JClass relativi alle classi del progetto
    NB: precedentemente i file vengono formattati con un nuovo stile
    per consentire un corretto riconoscimento da parte del sistema di parsing
    '''
    def getAndroidProjectClasses(self):
        # prima di creare la lista di oggetti per ogni classe formatto il testo
        for p in self.android_project_classes_paths:
            adah_util.formatter(p)
            
        # ritorno la lista degli oggetti generati dai file formattati
        res = []
        paths = []
        for p in self.android_project_classes_paths:
            jc_temp = JClass(p)
            if(jc_temp.class_node):
                res.append(jc_temp)
                paths.append(jc_temp.class_path)
        return res, paths
    
    '''
    funzione che ritorna la lista di oggetti JClass relativi alle Activity presenti nel progetto
    e la lista dei loro path
    '''
    def getAndroidProjectActivityClasses(self):
        res = []
        for jc in self.android_project_classes:
            if(jc.is_activity):
                res.append(jc)
        return res
    
    '''
    rendo la classe una stringa
    '''
    def __str__(self):
        res = "\tAndroid project path: \n" + self.android_project_path
        res += "\n\n\tChecksum function template: \n" + self.original_checksum_function_template
        res += "\n\n\tChecksum function imports: \n" + self.original_checksum_function_imports
        res += "\n\n\tAndroid valid pngs paths: \n" + "\n".join(self.android_valid_pngs_paths)
        res += "\n\n\tAndroid project classes paths: \n" + "\n".join(self.android_project_classes_paths)
        res += "\n\n\tAndroid project classes RCF injection points: \n" + "\n".join("\n" + str(s) for s in self.android_project_classes_rcf_injection_points)
        res += "\n\n\tAndroid project pngs paths: \n" + "\n".join(self.android_project_pngs_paths)
        res += "\n\n\tAndroid injected pngs paths: \n" + "\n".join(self.android_injected_pngs_paths)
        res += "\n\n\tRandom checksum functions descriptors: \n" + "\n".join("".join(str(s)) for s in self.random_checksum_functions_descriptors)
        res += "\n\n\tActivity Injection points: \n" + "\n".join("\n" + str(s) for s in self.activity_rcf_call_injection_points)
        res += "\n\n\tRCF Injection Descriptors: \n" + "\n".join("".join(str(s)) for s in self.rcf_injection_descriptors)
                 
        return res
    
