'''
Created on 15 set 2017

@author Matteo Olivato <matteo.olivato@studenti.univr.it>
@author Federico Bianchi <federico.bianchi@studenti.univr.it>

'''
import shutil
import os

class InjectionDescriptor(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        
        # classe java in cui sarà iniettata la funzione RCF
        self.jclassRCFInjectionPoint = None
        
        # classe java in cui sarà iniettata la chiamata alla funzione RCF
        self.jmethodCallToRCFInjectionPoint = None
        
        # descrittore per la generazione della funzione RCF
        self.RCFDescriptor = []
        
        # file png originale da copiare per creare il file png usato per il salvataggio del Checksum
        self.originalPngFile = None
        
        # file png originale da copiare per creare il file png usato per il salvataggio del Checksum
        self.injectedPngFile = None
        
        # file png originale da copiare per creare il file png usato per il salvataggio del Checksum
        self.backupJFile = False
    
    '''
    funzione che inietta effettivamente del codice all'interno di un file
    '''
    def jfile_injector(self, text, iline, tpayload):
        return "\n".join(text.splitlines()[:iline]) + "\n" + tpayload + "\n".join(text.splitlines()[iline:])
    
    '''
    funzione che esegue l'iniezione temporanea modificando gli oggetti
    '''
    def inject(self, rcf_text, rcf_imports):
        # ottengo gli imports modificati dall'iniezione
        temp_jimports = list(set(rcf_imports + self.jclassRCFInjectionPoint.class_imports))
        temp_jimports.sort()
        
        # ottengo il body modificato dall'iniezione della RCF
        text = self.jclassRCFInjectionPoint.class_body["text"]
        iline = self.jclassRCFInjectionPoint.class_body["end_row"] - self.jclassRCFInjectionPoint.class_body["start_row"] - 1
        tpayload = rcf_text
        temp_jbody = self.jfile_injector(text, iline , tpayload)
        
        # aggiorno i dati del jclassRCFInjectionPoint
        self.jclassRCFInjectionPoint.class_body["end_row"] += len(rcf_text.splitlines())
        self.jclassRCFInjectionPoint.class_body["text"] = temp_jbody
        self.jclassRCFInjectionPoint.class_imports = temp_jimports
        
        # ottengo il body modificato dall'iniezione della chiamata alla RCF
        text = self.jmethodCallToRCFInjectionPoint.method_class.class_body["text"]
        iline = self.jmethodCallToRCFInjectionPoint.method_body["end_row"] - self.jmethodCallToRCFInjectionPoint.method_class.class_body["start_row"] - 1
        # creo il payload per la chiamata alla funzione RCF appena iniettata
        # TODO: gestire la chiamata tramite contruttore di oggetto anonimo evitando lo static nella funzione matrice della RCF
        tpayload = "        " + self.jclassRCFInjectionPoint.class_name + "." + self.RCFDescriptor[0] + "(this);\n"
        temp_calljbody = self.jfile_injector(text, iline , tpayload)
        
        # aggiorno i dati della classe che contiene il jmethodCallToRCFInjectionPoint
        self.jmethodCallToRCFInjectionPoint.method_class.class_body["text"] = temp_calljbody
        self.jmethodCallToRCFInjectionPoint.method_class.class_body["end_row"] += 1 
        
        # creo il path per il file png da iniettare
        png_path_list = self.originalPngFile.split("/")
        self.injectedPngFile = "/".join(png_path_list[:len(png_path_list) - 1]) + "/" + self.RCFDescriptor[3]
    
    '''
    funzione che salva nei file java effettivamente le modifiche apportate dopo l'iniezione negli oggetti
    '''
    def saveInjection(self):
        
        # eseguo il backup dei file prima di modificarli
        self.backupMe()
        
        # salvo il file che contiene la funzione RCF
        jcRCFIP = open(self.jclassRCFInjectionPoint.class_path, "w")
        print(self.jclassRCFInjectionPoint.class_path)
        jcRCFIP.write("\npackage " + self.jclassRCFInjectionPoint.class_package + ";" + "\n\n")
        jcRCFIP.write("\n".join(self.jclassRCFInjectionPoint.class_imports) + "\n\n")
        jcRCFIP.write(self.jclassRCFInjectionPoint.class_signature + "\n")
        jcRCFIP.write(self.jclassRCFInjectionPoint.class_body["text"])
        jcRCFIP.close()
        
        # salvo il file che contiene la chiamata alla funzione RCF
        jcRCFIP = open(self.jmethodCallToRCFInjectionPoint.method_class.class_path, "w")
        print(self.jmethodCallToRCFInjectionPoint.method_class.class_path)
        jcRCFIP.write("\npackage " + self.jmethodCallToRCFInjectionPoint.method_class.class_package + ";" + "\n\n")
        
        # inserisco l'import per la gestione corretta della chiamata alla RCF
        self.jmethodCallToRCFInjectionPoint.method_class.class_imports += ["import " + self.jclassRCFInjectionPoint.class_package + "." + self.jclassRCFInjectionPoint.class_name + ";"]
        jcRCFIP.write("\n".join(self.jmethodCallToRCFInjectionPoint.method_class.class_imports) + "\n\n")
        jcRCFIP.write(self.jmethodCallToRCFInjectionPoint.method_class.class_signature + "\n")
        jcRCFIP.write(self.jmethodCallToRCFInjectionPoint.method_class.class_body["text"])
        jcRCFIP.close()
        
        # salvo il file png che conterrà il checksum del file dex
        shutil.copyfile(self.originalPngFile, self.injectedPngFile)
        print(self.injectedPngFile)
        
        return
    
    '''
    funzione che esegue un backup dei file attuali per poter ripristinare le condizioni iniziali precedenti all'iniezione
    '''
    def backupMe(self):
        # eseguo il backup del file che conterrà la funzione RCF
        if not os.path.exists(self.jclassRCFInjectionPoint.class_path + ".adah_backup"):
            shutil.copyfile(self.jclassRCFInjectionPoint.class_path, self.jclassRCFInjectionPoint.class_path + ".adah_backup")
        # eseguo il backup del file che conterrà la chiamata alla mia funzione RCF
        if not os.path.exists(self.jmethodCallToRCFInjectionPoint.method_class.class_path + ".adah_backup"):
            shutil.copyfile(self.jmethodCallToRCFInjectionPoint.method_class.class_path, self.jmethodCallToRCFInjectionPoint.method_class.class_path + ".adah_backup")
    
    '''
    funzione che cancella i backup dei file java precedenti all'iniezione
    '''
    def deleteBackups(self):
        # rimuovo il backup del file che della funzione RCF
        if os.path.exists(self.jclassRCFInjectionPoint.class_path + ".adah_backup"):
            os.remove(self.jclassRCFInjectionPoint.class_path, self.jclassRCFInjectionPoint.class_path + ".adah_backup")
        # rimuovo il backup del file della chiamata alla mia funzione RCF
        if os.path.exists(self.jmethodCallToRCFInjectionPoint.method_class.class_path + ".adah_backup"):
            os.remove(self.jmethodCallToRCFInjectionPoint.method_class.class_path, self.jmethodCallToRCFInjectionPoint.method_class.class_path + ".adah_backup")
    
    
    '''
    rendo la classe una stringa
    '''
    def __str__(self):
        res = "\tJClass RCF Injection Point: \n" + str(self.jclassRCFInjectionPoint)
        res += "\n\n\tJClass Call To RCF Injection Point: \n" + str(self.jmethodCallToRCFInjectionPoint)
        res += "\n\n\tRCF Descriptor: \n" + str(self.RCFDescriptor)
        res += "\n\n\tOriginal Png File: \n" + str(self.originalPngFile)
        res += "\n\n\tInjected Png File: \n" + str(self.injectedPngFile)
        
        return res
