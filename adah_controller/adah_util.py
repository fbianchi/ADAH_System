'''
Created on 12 set 2017

@author Matteo Olivato <matteo.olivato@studenti.univr.it>
@author Federico Bianchi <federico.bianchi@studenti.univr.it>

'''

''' funzione che filtra una lista di stringhe con le seguenti proprietà:
    mainlist = lista di stringhe da filtrare
    allmatch = le stringhe che devono essere presenti contemporaneamente nella stringa
    anymatch = le stringhe che possono essere presenti nella stringa
    allnotmatch = le stringhe che non devono essere contemporaneamente presenti nella stringa
    anynotmatch = le stringhe che non possono essere presenti nella stringa
'''
def strings_list_positive_filter(mainlist, allmatch=[""], anymatch=[""]):
    return [x for x in mainlist if (all(y in x for y in allmatch) and any(y in x for y in anymatch))]

''' funzione che filtra una lista di stringhe con le seguenti proprietà:
    mainlist = lista di stringhe da filtrare
    allnotmatch = le stringhe che non devono essere contemporaneamente presenti nella stringa
'''
def strings_list_negative_filter(mainlist, allnotmatch):
    return [x for x in mainlist if (all(y not in x for y in allnotmatch))]

'''
funzione che ordina i modificatori di un metodo o di una classe
1° public, private, protected
2° static
3° syncronized
4° final
'''
def sortModifiers(node):
    modifiers_list = list(node.modifiers)
    modifiers_list.sort()
    res = " ".join(modifiers_list)
    
    if("final " in res):
        res = res.replace("final ", "")
        res += " final" 
    
    return res

'''
funzione che formatta i codici sorgenti del progetto
per una corretta elaborazione da parte del parser
'''
def formatter(jclass_path):
        # pulisco il file di origine da dichiarazioni di metodi spezzate su più righe
        text = open(jclass_path, "r").read()
            
        text = "\n".join(" ".join(x.split()) for x in text.replace(",\n", ",").splitlines()).replace(", ", ",")
        text = text.replace(",\n", ", ")
        # text = text.replace("(\n","(")
        # text = text.replace("\n)",")")
        open(jclass_path, "w").write(text)

            
        # utilizzo il programma astyle per ridefinire lo stile del file java
        # command = "astyle --style=horstmann -F -U -xg -k1 -W1 -y -e -xb -xp " + jclass_path
        command = "astyle --mode=java --style=horstmann -U -xG " + jclass_path
        import os
        os.system(command)
        
        # text = open(jclass_path, "r").read()
        # text = text.replace(",\n",",")
        # text = text.replace("(\n","(")
        # text = text.replace("\n)",")")
        # open(jclass_path, "w").write(text)

''' 
funzione che sostituisce i commenti nel file java con degli spazi
per effettuare il corretto conteggio delle parentesi
TODO: da correggere -- funzione inutile se si usa un parser migliore --
'''
def remove_comments(self, file_op_r):
    res = []
    for x in file_op_r.splitlines():
        if (x.find("//") > -1):
            a = x.find("//")
            b = a + x[a:].find("\n")
            res.append(x[:a] + " " * (len(x[a:b]) + 1))
        else:
            res.append(x)

    return "\n".join(res)

'''
funzione che recupera la riga di inizio e di fine del corpo di un metodo o di una calasse java
utilizza il metodo del conteggio delle parentesi graffe.
TODO: modificare il settaggio del file di testo mettendo le dichiarazioni di metodi tutti su una riga
TODO: pulire il file dal eventuali commenti inopportuni (che contengono graffe che modificano il conteggio)
'''
def bodyCollector(text, prefix_signature):
        
        file_op_list = text.splitlines()
        result = ""
        offset = 0
        
        for n, row in enumerate(file_op_list):
            if (all(x in row for x in prefix_signature.split(" "))):
                offset = n
                file_op_r = "\n".join(file_op_list[n:])
                # no_comments_old = self.remove_comments(file_op_r)
                
                no_comments = file_op_r
                count = 0
                index_line = 0
                start_body = 0
                end_body = 0
                for index, c in enumerate(no_comments):

                    if (c == "\n"):
                        index_line = index

                    if (c == "{"):
                        if (count == 0):
                            start_body = index_line + 1
                        count = count + 1

                    if (c == "}"):
                        count = count - 1
                        if (count == 0):
                            end_body = index
                            break
                result = file_op_r[start_body:end_body + 1]

        return { "start_row" : offset, "end_row" : offset + len(result.splitlines()), "text" : result}
