'''
Created on 12 set 2017

@author Matteo Olivato <matteo.olivato@studenti.univr.it>
@author Federico Bianchi <federico.bianchi@studenti.univr.it>

'''
from adah_controller import adah_util

class JField(object):
    '''
    classdocs
    '''

    def __init__(self, node, jclass):
        # mi salvo la classe che contiene l'attributo
        self.method_class = jclass
        # mi salvo il nodo dell'attributo
        self.field_node = node
        # mi salvo il nome dell'attributo
        self.field_name = self.field_node.declarators[0].name
        # mi salvo i modificatori dell' attributo
        self.field_modifiers = adah_util.sortModifiers(self.field_node)
        # mi salvo il tipo dell' attributo
        self.field_type = self.field_node.type.name
        # recupero l'eventuale valore di inizializzazione
        self.field_init = self.getInitializerValue()
    
    # recupero il valore di inizializzaizone dell'attributo se presente scorrendo l'albero di parsing
    # TODO: gestire eventuali dichiarazioni di Array o Oggetti, togliere Literal 
    def getInitializerValue(self):
        if (self.field_node.declarators[0].initializer):
            if (str(self.field_node.declarators[0].initializer) == "Literal"):
                return str(self.field_node.declarators[0].initializer.value)
        
        return None
    
    '''
    rendo la classe una stringa
    '''
    def __str__(self):
        res = "\tJField node: " + str(self.field_node)
        res += "\n\tJField modifiers: " + self.field_modifiers
        res += "\n\tJField type: " + self.field_type
        res += "\n\tJField name: " + self.field_name
        res += "\n\tJField init value: " + str(self.field_init)
                
        return res