#!/bin/bash

###
### Author: Matteo Olivato <matteo.olivato@studenti.univr.it>
### Author: Federico Bianchi <federico.bianchi@studenti.univr.it>
### Date: 18/11/2017
###

# astyle, pip3 and tkinter installation on Ubuntu
sudo apt install astyle python3-pip python3-tk

# pip3 package upgrade installation
sudo pip3 install --upgrade pip
# pip3 install appJar
sudo pip3 install appJar
