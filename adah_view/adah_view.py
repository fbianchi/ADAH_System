'''
Created on 21 set 2017


@author Matteo Olivato <matteo.olivato@studenti.univr.it>
@author Federico Bianchi <federico.bianchi@studenti.univr.it>

'''

import sys
from appJar import gui
from adah_model.adah_model import AdahModel
from adah_controller.adah_controller import AdahController

class AdahView(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        # create a GUI variable called app
        self.app = gui("ADAH System", "960x720")
        self.app.setFont(11)
        
        # add & configure widgets - widgets get a name, to help referencing them later
        self.app.addLabel("title", "Welcome to ADAH System", colspan=4)
            
        # separatore orizzontale 
        self.app.addHorizontalSeparator(colour="black", colspan=4)
        
        ########### Sono nello spazio dedicato all'Injector
        
        self.app.addLabel("aprjpath_lb", "Android Project Path: ", colspan=4)
        self.app.addDirectoryEntry("android_project_path", colspan=4)
        
        self.app.addButtons(["Load Project", "Reset Project"], self.load, colspan=4)
        
        self.app.addLabel("pstatus_lb", "Project Status: Unloaded", colspan=4)
        self.app.setLabelBg("pstatus_lb", "lightblue")
        
        # separatore orizzontale 
        self.app.addHorizontalSeparator(colour="black", colspan=4)
        
        self.app.addLabel("inopt_lb", "Injector Options: ", colspan=4)
        
        row = self.app.getRow()
        # checkbox che indica se nelle funzioni di checkusm devo inserire i comandi che stampano i log
        self.app.addCheckBox("Log commands", row, 0)
        # checkbox che indica se nelle funzioni di checksum devo inserire i commenti testuali
        self.app.addCheckBox("Comments", row, 1)
        # spinbox che indica il numero di funzioni di checksum che saranno chiamate dall'applicazione
        self.app.addLabel("nrcf_lb", "Number of RCF : ", row, 2)
        self.app.addSpinBoxRange("Num RCF Functions", 1, 20, row, 3)
        
        self.app.addButton("Inject", self.inject, colspan=4)
        
        self.app.addLabel("istatus_lb", "Injection Status: Clear", colspan=4)
        self.app.setLabelBg("istatus_lb", "lightblue")
        
        # separatore orizzontale 
        self.app.addHorizontalSeparator(colour="black", colspan=4)
        
        ########### Sono nello spazio dedicato al Cleaner
            
        self.app.addLabel("aiprpath_lb", "Android Injected Project Path: ", colspan=4)
        #self.app.addDirectoryEntry("android_injected_project_path", colspan=4)
        
        self.app.addButton("Clear Project", self.clear, colspan=4)
        
        self.app.addLabel("cstatus_lb", "Clear Status: Unknown", colspan=4)
        self.app.setLabelBg("cstatus_lb", "lightblue")
        
        # separatore orizzontale 
        self.app.addHorizontalSeparator(colour="black", colspan=4)
        
        ########### Sono nello spazio dedicato al Signer
        self.app.addLabel("aapkpath_lb", "Android APK Path: ", colspan=4)
        self.app.addFileEntry("android_apk_path", colspan=4)
        
        self.app.addButton("Sign APK", self.sign, colspan=4)
         
        self.app.addLabel("sstatus_lb", "Sign Status: Unsigned", colspan=4)
        self.app.setLabelBg("sstatus_lb", "lightblue")
        
    
    '''
    funzione che carica il progetto inserito
    '''
    def load(self, button):
        try:
            if button == "Reset Project":
                
                # TODO: pulire il progetto dall'iniezione oppure rendere l'iniezione multipla (più steps) funzionante
                
                # resetto le label
                self.app.setLabel("pstatus_lb", "Project Status: Unloaded")
                self.app.setLabelBg("pstatus_lb", "lightblue")
                
                self.app.setLabel("istatus_lb", "Injection Status: Clear")
                self.app.setLabelBg("istatus_lb", "lightblue")
                
                self.app.setLabel("cstatus_lb", "Clear Status: Unknown")
                self.app.setLabelBg("cstatus_lb", "lightblue")
                
                self.app.setLabel("sstatus_lb", "Sign Status: Unsigned")
                self.app.setLabelBg("sstatus_lb", "lightblue")
                
                # resetto model e controller
                self.model = None
                self.controller = None
                
            else:
                # se non ho inserito nulla termino
                if (len(self.app.getEntry("android_project_path")) <= 0):
                    return
                # costruisco il model e il controller
                self.model = AdahModel(self.app.getEntry("android_project_path"), "atcf_base/atcf_base_code.txt", "atcf_base/atcf_base_imports.txt")
                self.controller = AdahController(self.model)
                
                # cambio il testo della label dello stato
                self.app.setLabel("pstatus_lb", "Project Status: Loaded")
                # setto la label dello status verde
                self.app.setLabelBg("pstatus_lb", "lightgreen")
        except:
            print("Unexpected error:", "\n".join(sys.exc_info()))
            # cambio il testo della label dello stato
            self.app.setLabel("pstatus_lb", "Project Status: Error")
            # setto la label dello status verde
            self.app.setLabelBg("pstatus_lb", "red")
            
    '''
    funzione che avvia la view
    '''
    def inject(self, button):
        try:
            # resetto le label
            self.app.setLabel("cstatus_lb", "Clear Status: Unknown")
            self.app.setLabelBg("cstatus_lb", "lightblue")
            
            self.app.setLabel("sstatus_lb", "Sign Status: Unsigned")
            self.app.setLabelBg("sstatus_lb", "lightblue")
            
            # costruisco le funzioni di checksum
            self.controller.genRandomChecksumFunctionsInjectionDescriptor(bool(self.app.getCheckBox("Log commands")), bool(self.app.getCheckBox("Comments")), int(self.app.getSpinBox("Num RCF Functions")))
            # inietto le modifiche contenute nei descrittori
            self.controller.injectDescriptors()
            # salvo il file dei descrittori per i png utilizzato durante la fase di firma
            self.controller.savePngFileInjectionDescriptors()
            # salvo le modifiche dell'iniezione sui file
            self.controller.saveInjectionDescriptors()
            
            # cambio il testo della label dello stato
            self.app.setLabel("istatus_lb", "Injection Status: ADAH Injected")
            # setto la label dello status verde
            self.app.setLabelBg("istatus_lb", "lightgreen")
        except:
            print("Unexpected error:", "\n".join(sys.exc_info()))
            # cambio il testo della label dello stato
            self.app.setLabel("istatus_lb", "Injection Status: Error")
            # setto la label dello status verde
            self.app.setLabelBg("istatus_lb", "red")
    '''
    funzione che pulisce il progetto
    '''
    def clear(self, button):
        try:
            # resetto le label
            self.app.setLabel("istatus_lb", "Injection Status: Clear")
            self.app.setLabelBg("istatus_lb", "lightblue")
            
            self.app.setLabel("sstatus_lb", "Sign Status: Unsigned")
            self.app.setLabelBg("sstatus_lb", "lightblue")
            
            # pulisco il progetto ripristinando i file di backup e cancellando i file png non validi
            self.controller.clearInjection()
            
            # cambio il testo della label dello stato
            self.app.setLabel("cstatus_lb", "Clear Status: Cleared")
            # setto la label dello status verde
            self.app.setLabelBg("cstatus_lb", "lightgreen")
        except:
            print("Unexpected error:", "\n".join(sys.exc_info()))
            # cambio il testo della label dello stato
            self.app.setLabel("cstatus_lb", "Clear Status: Error")
            # setto la label dello status verde
            self.app.setLabelBg("cstatus_lb", "red")
        
        
    '''
    funzione che firma i png con la prima build dell'apk
    '''
    def sign(self, button):
        try:
            # se non ho inserito nulla termino
            if (len(self.app.getEntry("android_apk_path")) <= 0):
                return
            # pulisco il progetto ripristinando i file di backup e cancellando i file png non validi
            self.controller.signAPK(self.app.getEntry("android_apk_path"))
            
            # cambio il testo della label dello stato
            self.app.setLabel("sstatus_lb", "Sign Status: Signed")
            # setto la label dello status verde
            self.app.setLabelBg("sstatus_lb", "lightgreen")
        except:
            print("Unexpected error:", "\n".join(sys.exc_info()))
            # cambio il testo della label dello stato
            self.app.setLabel("sstatus_lb", "Sign Status: Error")
            # setto la label dello status verde
            self.app.setLabelBg("sstatus_lb", "red")
            
    def go(self):
        self.app.go()
        