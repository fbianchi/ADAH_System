'''
Created on 23 ago 2017

@author Matteo Olivato <matteo.olivato@studenti.univr.it>
@author Federico Bianchi <federico.bianchi@studenti.univr.it>

'''
import os
import glob
import random
import string
import hashlib
import zipfile

from PIL import Image

from adah_model.InjectionDescriptor import InjectionDescriptor


class AdahController(object):
    '''
    classdocs
    '''
    def __init__(self, adahM):
        '''
        Constructor
        '''
        # inserisco il modello da utilizzare
        self.model = adahM;
        # trovo i punti di iniezione della nuova funzione RCF
        self.findRCFInjectionPoints()
        
        # trovo i possibili punti di iniezione per la chiamata alla RCF
        # NB: per adesso uso solo le funzioni onCreate
        self.findCallToRCFInjectionPoints()
        
    '''
    funzione che trova i punti di iniezione per la chiamata alla RCF,
    ovvero i metodi onCreate, nelle classi android che sono activity
    '''
    def findCallToRCFInjectionPoints(self):
        for ajc in self.model.android_project_activity_classes:
            # aggiungo tutti i metodi oncreate del progetto
            self.model.activity_rcf_call_injection_points += ajc.getJMethodsByName("onCreate")
    
    '''
    funzione che trova i punti di iniezione per le funzioni RCF
    '''
    def findRCFInjectionPoints(self):
        for _ in range(len(self.model.random_checksum_functions_descriptors)):
            # scelgo random la calsse in cui iniettare il codice della funzione RCF
            self.model.android_project_classes_rcf_injection_points.append(random.choice(self.model.android_project_classes))
    
    ''' funzione che costruisce la funzione android che esegue il controllo antitampering del checksum '''

    def buildRandomChecksumFunction(self, function_name, return_type, return_value, img_checksum_name, hash_alg_name, hash_alg_length, log_flag, comments_flag):
        # file che contiene il template base per la funzione di checksum
        o_fcheck = open(self.model.original_checksum_function_template , 'r').read().splitlines()
    
        if not log_flag:
            # tolgo dalla funzione i comandi di Log
            o_fcheck = [s for s in o_fcheck if "Log.d" not in s]
    
        if not comments_flag:
            # tolgo dalla funzione i commenti in italiano
            o_fcheck = [s for s in o_fcheck if "// " not in s]
    
        output_fcheck = "\n".join(o_fcheck)
    
        # creo il dizionario per l'inserimento dei valori scelti nel testo della funzione android
        d = dict(check_function_name = function_name,
                 fun_return_type = return_type, 
                 fun_return_value = return_value,
                 img_checksum_name = "\"" + img_checksum_name + "\"",
                 hash_alg_name = "\"" + hash_alg_name + "\"",
                 hash_alg_length = hash_alg_length)
        
        # ritorno il testo della funzione con i placeholder sostituiti dai valori del dizionario
        return string.Template(output_fcheck).safe_substitute(d)
    
    ''' funzione che sceglie i parametri per generare la funzione di checksum
        ovvero >>
        - nome della funzione: 1) dalla lista dei nomi di funzioni per quella classe (polimorfismo)
                               2) dalla lista dei nomi delle funzioni della classe object (polimorfismo)
        - tipo di ritorno della funzione: tipi presi dal punto precedente
        - valore di ritorno della funzione: elemento casuale del tipo del punto precedente
        - nome dell'immagine che conterrà il checksum: nome casuale (possibilmente simile alle immagini nella sua cartella)
        - funzione di hash usata per il calcolo del checksum: {'md5', 'sha1', 'sha224', 'sha256', 'sha384', 'sha512'}
        - lunghezza del digest esadecimale dell'hash precedente: {32, 40, 56, 64, 96, 128}
        - flag che indica la presenza delle stringhe di Log: {True, False} -> usato per il debug
        - flag che indica la presenza dei commenti: {True, False} -> usato per il debug
        TODO: gestire tipi di ritorno complessi (ArrayList, Set, Map, List ecc. in generale i tipi generici con parentesi angolari)
        TODO: evitare di utilizzare metodi che abbiano già come parametro un'Activity
    '''

    def genRandomChecksumFunctionsSignature(self, log_flag, comments_flag, defined_functions=()):
        # definisco la lista di tuple per l'algoritmo di hash utilizzabile:
        # (nome_algoritmo, lunghezza_digest)
        l_hash_algorithms = [("MD5", 32),
                             ("SHA-1", 40),
                             ("SHA-224", 56),
                             ("SHA-256", 64),
                             ("SHA-384", 96),
                             ("SHA-512", 128)]
    
        # definisco la lista di tuple per le funzioni della classe object (eventuale polimorfismo o override)
        # (nome, tipo, valore di ritorno di default)
        l_object_functions = [("equals", "boolean"),
                              ("getClass", "Class"),
                              ("hashCode", "int"),
                              ("notify", "void"),
                              ("notifyAll", "void"),
                              ("toString", "String"),
                              ("wait", "void"),
                              ("wait", "void")]
    
        # dizionario che ritorna i valori di ritono di default pe il tipo di funzione scelto
        default_val = {"boolean": "false",
                       "byte": "0",
                       "int": "0",
                       "short": "0",
                       "long": "0",
                       "float": "0",
                       "double": "0",
                       "char": "'a'",
                       "void": "",
                       "String": "\"\""}
    
        # creo la lista delle funzioni disponibili di cui fare polimorfismo
        l_functions = list(set(l_object_functions) ^ set(defined_functions))
    
        # creo il nome per il file png casualmente
        png_name = "".join(random.choice("".join(c for c in string.ascii_lowercase if c not in "aeioux")) + random.choice("_aeiou") for _ in range(random.randint(8, 14) // 3))
    
        # scelgo random l'algoritmo di hash da utilizzare
        hash_algorithm = random.choice(l_hash_algorithms);
    
        # scelgo random la funzione di cui fare polimorfismo
        check_fun_name = random.choice(l_functions)
        
        # ritorno la lista di signature e opzioni delle RCF che verranno utilizzate durante l'iniezione e generazione
        return list(check_fun_name) + [default_val.setdefault(check_fun_name[1], "null")] + [png_name + ".png"] + list(hash_algorithm) + [log_flag, comments_flag]  
    
    ''' funzione che genera N diverse funzioni di checksum '''

    def genRandomChecksumFunctionsInjectionDescriptor(self, log_flag, comments_flag, number=1):
        # ogni volta che chiamo questa funzione resetto i descriptors
        self.model.injection_descriptors_list = []
        # trovo i punti di iniezione della nuova funzione RCF
        self.findRCFInjectionPoints()
        
        # trovo i possibili punti di iniezione per la chiamata alla RCF
        # NB: per adesso uso solo le funzioni onCreate
        self.findCallToRCFInjectionPoints()
        
        for _ in range(number):
            # creo l'oggetto che rappresenta il descrittore dell'iniezione
            rcfid = InjectionDescriptor()
            # scelgo random la classe java in cui iniettare la funzione RCF
            rcfid.jclassRCFInjectionPoint = random.choice(self.model.android_project_classes)
            # scelgo random la classe java in cui nietterò la chiamata alla funzione RCF (ovvero un'activity)
            rcfid.jmethodCallToRCFInjectionPoint = random.choice(self.model.activity_rcf_call_injection_points)
            # genero random la signature per il metodo sfruttando il polimorfismo
            defined_functions = ((jm.method_name, jm.method_return_type) for jm in rcfid.jclassRCFInjectionPoint.methods_list)
            
            # fenero le signature delle funzioni RCF
            rcfid.RCFDescriptor = self.genRandomChecksumFunctionsSignature(log_flag, comments_flag, defined_functions)
            # scelgo random il png originale da copiare per salvataggio del checksum
            rcfid.originalPngFile = random.choice(self.model.android_project_pngs_paths)
            # salvo il descrittore dell'iniezione
            self.model.injection_descriptors_list.append(rcfid) 
    
    '''
    funzione che esegue l'iniezione modificando i vari InjectionDescriptor:
    1°) Inietto la funzione RCF nel file della classe
    2°) Inietto il file png che sarà utilizzato da questa funzione come verifica per il checksum
    3°) Inietto la chiamata alla funzione RCF appena iniettata
    NB: i file non sono ancora stati modificati (e il file png non è stato realmente iniettato),
    sono stati solo modificati i Descrittori.
    '''

    def injectDescriptors(self):
        for rcfid in self.model.injection_descriptors_list:
            # chiamo la funzione di injectionDescriptor che esegue le modifiche nell'oggetto
            rcfid.inject(self.buildRandomChecksumFunction(*rcfid.RCFDescriptor), open(self.model.original_checksum_function_imports).read().splitlines())
    
    '''
    funzione che salva il file con i descrittori dei file png e relativi hash,
    questo file è utilizzato durante la fase di firma dell'apk
    '''

    def savePngFileInjectionDescriptors(self):
        pfid = open(self.model.png_file_injection_descriptors, "w")
        for rcfid in self.model.injection_descriptors_list:
            pfid.write(", ".join([rcfid.originalPngFile, rcfid.injectedPngFile, rcfid.RCFDescriptor[4].replace("-", "").lower()]) + "\n")
            
    '''
    funzione che salva le modifiche dell'InjectorDescritor sui file java del progetto
    '''

    def saveInjectionDescriptors(self):
        for rcfid in self.model.injection_descriptors_list:
            rcfid.saveInjection()
            
    '''
    funzione che resetta i file del progetto ripristinando i file di backup
    '''

    def clearInjection(self):
        
        # trovo i file con estensione adah_backup
        backed_up_jfiles = [y for x in os.walk(self.model.android_project_path) for y in glob.glob(os.path.join(x[0], '*.adah_backup')) if all(z not in y for z in self.model.android_invalid_classes_paths)]
        
        # se non ho trovato file termino
        if len(backed_up_jfiles) == 0:
            print("Il progetto non contiene file di backup!")
            return
        
        # cancello i file modificati
        for bujf in backed_up_jfiles:
            os.remove(bujf.replace(".adah_backup", ""))
            print(bujf.replace(".adah_backup", ""))
        
        # ripristino i file originali rinominando i file di backup
        for bujf in backed_up_jfiles:
            os.rename(bujf, bujf.replace(".adah_backup", ""))
        
        # cancello i png iniettati
        pfid = open(self.model.png_file_injection_descriptors, "r").read()
        for rcfid in pfid.splitlines():
            os.remove(rcfid.split(", ")[1])
            print(rcfid.split(", ")[1])
    
    '''
    funzione che ritorna il digest di un dato file
    '''

    def calc_digest_flist(self, zf_path, hash_name):
        zf = zipfile.ZipFile(zf_path)
        zflist = zf.namelist()
    
        # recupero solo i file .dex
        zflist = [s for s in zflist if ".dex" in s]
        print("\n".join(zflist))
    
        # calcolo l'hash comunitario di tutti i file
        hasher = hashlib.new(hash_name)
        hasher.update(zf.open(zflist[0], 'r').read())
        for fname in zflist[1:]:
            hasher.update(zf.open(fname, 'r').read())
        return hasher.hexdigest()
    
    '''
    funzione che salva l'hash di un file dentro un png
    '''

    def steganography_encrypt(self, imagepath, digest, imagepath_out):
        # trasformo il digest in una lista di interi di 8 bit
        ibl_dig = [int(digest[i * 2:i * 2 + 2], 16) for i in range(0, len(digest) // 2)]
        # trasformo la lista di interi in tuple che rappresentano i pixel dell'immagine
        tibl_dig = [tuple(ibl_dig[i * 4:i * 4 + 4]) for i in range(0, len(ibl_dig) // 4)]
        print(imagepath + ":" + "".join(str(tibl_dig)))
    
        # salvo il digest nei primi pixel verticali dell'immagine
        print(digest)
        img = Image.open(imagepath)
        # carico i pixel dell'immagine nella matrice
        pixels = img.load()
        for i in range(0, len(tibl_dig)):
            if tibl_dig[i][3] != 0:
                pixels[i, 0] = tibl_dig[i];
            else:
                pixels[i, 1] = (tibl_dig[i][0] , tibl_dig[i][1] , tibl_dig[i][2], 1);
                
        # salvo il file png con il checksum
        img.save(imagepath_out)
        img.close()
    
    '''
    funzione che firma i vari file png con i checksums dei file dex contenuti nell'APK
    '''

    def signAPK(self, apk_path):
        # file contenente le associazioni <nome_file.png, algoritmo_di_hash>
        paha_path = self.model.png_file_injection_descriptors
        # dizionario nome hash lunghezza in caratteri del digest
        hl_dict = {'md5': 32, 'sha1': 40, 'sha224': 56, 'sha256': 64, 'sha384': 96, 'sha512': 128}
        
        # lista dei <path_del_file_png, alias_png_file, algoritmo_di_hash>
        with open(paha_path) as f:
            paha = f.readlines()
        paha = [x.strip().split(', ') for x in paha]
        print(paha)
        
        # creo i file png stenografati con gli alias richiesti e verifico che il digest sia riconosciuto
        for i in paha:
            ''' TODO: una volta che ho nel file paha le directory corrette del progetto android devo sostituire
                <output_pngs_path> con <i[0]> '''
            # steganografo nei file png il relativo hash richiesto dalla paha e cambio il nome con l'alias
            self.steganography_encrypt(i[0], self.calc_digest_flist(apk_path, i[2]) , i[1])
            # stampo l'hash steganografato per vedere se è uguale
            print(self.steganography_decrypt(i[1], hl_dict[i[2]]))
    
    '''
    funzione che trova l'hash dentro una fotografia png
    '''

    def steganography_decrypt(self, imagepath, len_hexdigest):
        img = Image.open(imagepath)
        # carico i pixel dell'immagine nella matrice
        pixels = img.load()
        print(imagepath + ":\n" + " \n".join(str(pixels[i, 0]) for i in range(0, len_hexdigest // 8)))
        # recupero il digest dell'hash dai pixel dell'immagine
        hexdigest = "".join('{0:02x}'.format(i) for j in [pixels[i, 0] for i in range(0, len_hexdigest // 8)] for i in j)
        img.close()
        return hexdigest
