DEPENDENCIES REQUIREMENTS:

for python: PIL, appJar (based on tkinter for the View) 
for Linux or Windows or Mac: astyle

ATTENTION: This dependecies are automatically installed if you run setup.sh script.

ATTENTION_2: Remove Instant app option from Android Studio before debug application

ATTENTION_3: If there are some problems after RCF injections, clean the app folder and redo the process. 

TODO:

>Parser:

0) Parser module too much simple and bugged. The project need a much better Java parser.

>Formatter:

0) Formatter based on astyle. Must be written a python indipendent formatter for Java Code
1) Signature of methods declaration all in one line

>Other:

1) Problem with graffiti brace during counting into body collector. Must be written a python comment sobstitutor
2) All RCF methods are static. Should be created an anonimous object every time we call it.
3) We always inject the RCF call into onCreate Activity methods randomly. Another methods should be available.
4) If the parser is fixed, astyle can be removed from the project.
