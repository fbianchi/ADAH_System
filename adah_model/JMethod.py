'''
Created on 12 set 2017

@author Matteo Olivato <matteo.olivato@studenti.univr.it>
@author Federico Bianchi <federico.bianchi@studenti.univr.it>

'''
from adah_controller import adah_util

class JMethod(object):
    '''
    classdocs
    '''


    def __init__(self, node, jclass):
        # mi salvo la classe che contiene il metodo
        self.method_class = jclass
        # mi salvo il nodo del metodo
        self.method_node = node
        # mi salvo il nome del metodo
        self.method_name = self.method_node.name
        # mi salvo i modificatori del metodo
        self.method_modifiers = adah_util.sortModifiers(self.method_node)
        # mi salvo il tipo di ritorno del metodo
        self.method_return_type = self.getReturnType()
        # mi salvo i parametri e i tipi del metodo
        self.method_parameters = self.getParameters()
        # mi salvo il tipo di eccezione che lancia il metodo
        self.method_throws = self.getThrows()
        # recupero la signature originale del metodo
        self.method_signature = self.getSignature(self.method_class.text)
        # recupero il corpo del metodo con riga di inizio e fine
        self.method_body = adah_util.bodyCollector(self.method_class.text, self.method_signature)
    
    '''
    funzione che restituisce il tipo di ritorno del metodo
    '''
    def getReturnType(self):
        if (self.method_node.return_type):
            return  self.method_node.return_type.name
        return "void"
    
    '''
    funzione che restituisce i parametri della funzione
    '''
    def getParameters(self):
        res = []
        for x in self.method_node.parameters:
            # res += [x.modifiers , x.type.name, x.name]
            if(x.modifiers):
                res += list(x.modifiers)
            
            res += [x.type.name, x.name]
        
        return list(set(res))
    
    '''
    funzione che restituisce il throws il metodo ne ha qualcuno presente
    '''
    def getThrows(self):
        if(self.method_node.throws):
            return str(self.method_node.throws)
        return None
    
    '''
    funzione che recupera dal file la signature reale del metodo
    '''
    def getSignature(self, jclass_text):
        # creo il filtro per trovare la signature reale del metodo
        allmatch = [self.method_name, self.method_return_type]
        if(self.method_modifiers):
            allmatch += [self.method_modifiers]
        if(len(self.method_parameters) > 0):
            allmatch += [str(x) for x in self.method_parameters]
        if(self.method_throws):
            allmatch += [self.method_throws]
        
        res = adah_util.strings_list_positive_filter(jclass_text.splitlines(), allmatch)
        # escludo le signature che non sono corrette
        allnotmatch = ["//", "new", "==", "if", "else", "="]
        res = adah_util.strings_list_negative_filter(res, allnotmatch)
        
        # se non trovo la riga che sto cercando
        if (not res or len(res) == 0):
            signature_template = self.method_modifiers + " " + self.method_return_type + " " + self.method_name + "("
            res = [" --- No method signature -- ", signature_template]
            
        return " ".join(res)
    
    '''
    rendo la classe una stringa
    '''
    def __str__(self):
        res = "\tJMethod node: " + str(self.method_node)
        res += "\n\tJMethod modifiers: " + self.method_modifiers
        res += "\n\tJMethod name: " + self.method_name
        res += "\n\tJMethod return_type: " + str(self.method_return_type)
        res += "\n\tJMethod parameters: " + str(self.method_parameters)
        res += "\n\tJMethod throws: " + str(self.method_throws)
        res += "\n\tJMethod signature: " + self.method_signature
        res += "\n\tJMethod body: " + str(self.method_body)
                
        return res
    
    
