'''
@author Matteo Olivato <matteo.olivato@studenti.univr.it>
@author Federico Bianchi <federico.bianchi@studenti.univr.it>

'''
import javalang

from adah_controller import adah_util
from adah_model.JField import JField
from adah_model.JMethod import JMethod

class JClass:

    def __init__(self, path):
        
        # mi salvo il path della classe
        self.class_path = path
        # mi salvo il testo del file
        self.text = open(path, "r").read()
        # lancio il parser sul file java e recupero l'albero di parsing
        self.parse_tree = javalang.parse.parse(self.text)
        # recupero il primo nodo di dichiarazione di classe
        self.class_node = self.getClassDeclarationNode()
        
        # se il file non contiene una classdeclaration termino
        if not self.class_node:
            return
        
        # recupero il package della classe
        self.class_package = self.parse_tree.package.name
        # recupero gli imports effettuati nella classe
        self.class_imports = self.getClassImports()
        # recupero ilnome della calsse
        self.class_name = self.class_node.name
        # recupero e ordino i modificatori della classe
        self.class_modfiers = adah_util.sortModifiers(self.class_node)
        # recupero le classi estese
        self.class_extends = self.getClassExtends()
        # indico se è o non è un'activity
        self.is_activity = self.isActivity()  
        
        # recupero la signature originale della classe
        self.class_signature = self.getSignature()
        
        # recupero dal testo il corpo della classe
        self.class_body = adah_util.bodyCollector(self.text, self.class_signature)
        
        # mi salvo le liste di oggetti JField e JMethod estratti dal parseMe
        self.fields_list, self.methods_list = self.parseMe()
    
    '''
    funzione che ritorna il nodo ClassDeclaration
    se è Enum o Inteface ritorna None
    '''
    def getClassDeclarationNode(self):
        jclass_node = None
        # trovo la prima classdeclaration nel file java
        for x in self.parse_tree.types:
            if ("ClassDeclaration" in str(x)):
                jclass_node = x
        return jclass_node
    
    '''
    funzione che ritorna la lista di imports per la classe
    '''
    def getClassImports(self):
        class_imports = []
        # scorro i nodi imports del file java
        for x in self.parse_tree.imports:
            # costruisco la stringa degli imports partendo dalle info del parser
            str_class_import = "import "
            if x.static :
                str_class_import += "static "
            
            str_class_import += str(x.path)
            
            if x.wildcard :
                str_class_import += ".*;"
            else:
                str_class_import += ";"
            
            class_imports.append(str_class_import)
        return class_imports
    
    '''
    funzione che recupera la classe estesa se presente
    '''
    def getClassExtends(self):
        # se estende delle classi le ritorno
        if (self.class_node.extends):
            return str(self.class_node.extends.name)
        return None
    
    '''
    funzione che ritorna True se la classe è un'Activity (o estende Activity), altrimenti False
    '''
    def isActivity(self):
        if(self.class_extends):
            return "Activity" in self.class_extends or "Activity" in self.class_name
        else:
            return "Activity" in self.class_name
    
    '''
    funzione che recupera dal file la signature reale della classe
    '''
    def getSignature(self):
        allmatch = [self.class_modfiers, " class ", self.class_name]
        if(self.class_extends):
            allmatch += [" extends ", self.class_extends]
            
        return " ".join(adah_util.strings_list_positive_filter(self.text.splitlines(), allmatch))
    
    '''
    funzione che ritorna dei JMethods cercando per nome del metodo
    '''
    def getJMethodsByName(self, name):
        return [jm for jm in self.methods_list if jm.method_name == name]
    
    '''
    funzione che estrae dall'albero di parsing gli attibuti e i metodi della classe più esterna
    '''
    def parseMe(self):
        # inizializzo dizionari e liste
        fields_list = []
        methods_list = []
             
        for x in self.class_node.body:
            if (type(x) != list and type(x) != tuple):
                    
                # popolo la lista degli attributi della classe con un dizionario per ogni attributo
                for path, node in x.filter(javalang.tree.FieldDeclaration):
                    # recupero solo gli attributi della classe più esterna
                    if (len(path) == 0):
                        fields_list.append(JField(node, self))
                        
                # popolo la lista dei metodi della classe con un dizionario per ogni metodo
                for path, node in x.filter(javalang.tree.MethodDeclaration):
                    # recupero i metodi della classe più esterna
                    if (len(path) == 0):
                        
                        # for x in method_signature:
                        #    if ((node.name + "(") in x):
                        #        method_signature = (' '.join(x.split()))
        
                        methods_list.append(JMethod(node, self))
        
        return fields_list, methods_list

    '''
    rendo la classe una stringa
    '''
    def __str__(self):
        res = "\tJClass path: " + self.class_path
        res += "\n\tJClass node: " + str(self.class_node)
        res += "\n\tJClass package: " + self.class_package
        res += "\n\tJClass package: " + str(self.class_imports)
        res += "\n\tJClass modifiers: " + self.class_modfiers
        res += "\n\tJClass name: " + self.class_name
        res += "\n\tJClass extends: " + str(self.class_extends)
        res += "\n\tJClass isActivity: " + str(self.is_activity)
        res += "\n\tJClass signature: " + self.class_signature
        res += "\n\tJClass body: " + str(self.class_body)
        # res += "\n\tJClass text: " + self.text
                
        return res
