'''
Created on 23 ago 2017

@author Matteo Olivato <matteo.olivato@studenti.univr.it>
@author Federico Bianchi <federico.bianchi@studenti.univr.it>
'''

from adah_view.adah_view import AdahView

if __name__ == '__main__':
    # creo la view dell'applicazione
    adahAPP = AdahView()
    # avvio l'applicazione
    adahAPP.go()
